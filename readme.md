## Epitech TV - API Gant

### Pré-requis

* PHP >= 5.4
* Pré-requis Laravel : http://laravel.com/docs/5.0#server-requirements
* Base de donnée MySQL

---
### Installation

```
git clone git@bitbucket.org:aerstrasbourg/api-gant.git
cd api-gant
cp .env.example .env
nano .env
```

* Passer `APP_DEBUG` à `false`
* Renseigner les informations de la base de donnée
* Renseigner le login epitech pour `INTRA_USER`
* Renseigner le mot de passe unix en base64 pour `INTRA_PASSWORD`

```
composer update // Téléchargement des modules de Laravel
chmod 0777 -R storage
php artisan migrate // Création des tables de la base de donnée
```

---
### Utilisation

L'API permet d'utiliser 3 routes différentes :

* `GET /refresh` : Actualise toutes les données de tous les modules et leurs projets.
* `GET /modules/{scolar_year}/{semester?}` : Récupère la liste des modules en fonction de leur `scolar_year` (année scolaire, 2014 pour 2014-2015) et de leur `semester` (semestre, 1 ou 3 par exemple, optionnel).
Exemple de données renvoyées :
```
[
  {
    "title": "B - Conférences - Validation Bachelor",
    "semester": 0,
    "start": "2014-09-01",
    "end": "2015-08-01",
    "end_register": null,
    "code": "B-EPI-350",
    "code_instance": "STG-0-1",
    "credits": "0",
    "already_imported": false,
    "last_update": null
  },
  {
    "title": "B2 - Initiation Administration Système",
    "semester": 2,
    "start": "2015-02-12",
    "end": "2015-06-01",
    "end_register": "2015-04-26",
    "code": "B-ADS-150",
    "code_instance": "STG-2-1",
    "credits": "4",
    "already_imported": false,
    "last_update": null
  },
  {
    "title": "B0 - Hack-a-rush",
    "semester": 0,
    "start": "2015-02-23",
    "end": "2015-08-30",
    "end_register": "2015-08-30",
    "code": "G-EPI-105",
    "code_instance": "STG-0-1",
    "credits": "3",
    "already_imported": false,
    "last_update": null
  }
]
```
* `POST /gant` : Récupère la liste des projets actifs, avec leur date de début et de fin, ainsi que leur progression en pourcentage, en fonction des données passées en paramètre.
Exemple de données à envoyer (à passer dans le champ `params`) :
```
{
   modules: [
      {
         code: "B-MUL-150",
         code_instance: "STG-2-1"
      },
      {
         code: "B-CPE-155",
         code_instance: "STG-2-1"
      },
      {
         code: "B-PSU-155",
         code_instance: "STG-2-1"
      },
      {
         code: "B-PAV-360",
         code_instance: "STG-4-1"
      }
   ]
}
```
Exemple de données renvoyées :
```
[
   {
       title: "Raytracer",
       start: "2015-03-16 08:42:00",
       end: "2015-06-07 23:42:00",
       percent: 85
   },
   {
       title: "Zappy",
       start: "2015-04-06 00:00:00",
       end: "2015-07-12 23:42:00",
       percent: 51
   },
   {
       title: "[C++][PROJET] 4/4 - Bomberman",
       start: "2015-04-27 00:00:00",
       end: "2015-06-14 23:42:00",
       percent: 60
   }
]
```