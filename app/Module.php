<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class Module extends Model
{
	protected $table = 'modules';
	public $timestamps = false;

	public function projects()
	{
		return ($this->hasMany('App\Project'));
	}

	public function purge()
	{
		$projects = Project::where('module_id', $this->id)->get();

		foreach ($projects as $project)
		{
			$project->delete();
		}
	}

	public function refresh($force = false)
	{
		$today = new \DateTime(date('Y-m-d'));
		if (!empty($this->last_update) && new \DateTime($this->last_update) < $today || $force == true)
		{
			$this->purge();

			$this->last_update = new \DateTime();
			$this->save();

			$year = date('Y');
			$year = $year % 2 == 0 ? $year : $year - 1;

			$intraConnector = new IntraConnector(env('INTRA_USER'), base64_decode(env('INTRA_PASSWORD')));

			$module = $intraConnector->getDatas("https://intra.epitech.eu/module/" . $year . "/" . $this->code . "/" . $this->code_instance . "/?format=json");
			$module = json_decode(preg_replace('#// Epitech JSON webservice ...#', '', $module['response']));

			$this->title = $module->title;
			$this->semester = $module->semester;
			$this->start = $module->begin;
			$this->end = $module->end;
			$this->end_register = $module->end_register;
			$this->credits = $module->credits;
			$this->save();

			$activities = !empty($module->activites) ? $module->activites : NULL;

			if (!empty($activities))
			{
				foreach ($activities as $k => &$activity)
				{
					if ($activity->is_projet && $activity->type_code == "proj")
					{
						$project = new Project();
						$project->module_id = $this->id;
						$project->title = $activity->title;
						$project->start = new \DateTime($activity->begin);
						$project->end = new \DateTime($activity->end);
						$project->save();
					}
				}
			}
		}
	}

}
