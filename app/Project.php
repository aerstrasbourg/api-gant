<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Module;

class Project extends Model
{
	protected $table = 'projects';
	public $timestamps = false;

	public function module()
	{
		return ($this->belongsTo('App\Module'));
	}

}
