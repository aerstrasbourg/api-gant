<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'cors'], function (Illuminate\Routing\Router $router)
{
	/* Return the gant based on params input */
	$router->post("/gant", ['as' => 'gant.show', 'uses' => 'GantController@show']);

	/* Refresh all projects for all the modules */
	$router->get("/refresh", ['as' => 'gant.refresh', 'uses' => 'GantController@refresh']);

	/* Return the list of all existing modules */
	$router->get("/modules/{scolar_year}/{semester?}", ['as' => 'modules.show', 'uses' => 'ModuleController@show'])
			 ->where('scolar_year', '[0-9]{4}')->where('semester', '[0-9]+');
});
