<?php namespace App\Http\Controllers;

use App;
use App\IntraConnector, App\Module;

class ModuleController extends Controller
{

	public function __construct()
	{
	}

	public function show($scolarYear, $semester = NULL)
	{
		$intraConnector = new IntraConnector(env('INTRA_USER'), base64_decode(env('INTRA_PASSWORD')));

		$modulesRaw = $intraConnector->getDatas("https://intra.epitech.eu/course/filter?format=json&location[]=FR/STG&course[]=bachelor/classic&course[]=bachelor/tek2ed&course[]=other&scolaryear[]=" . $scolarYear);
		$modulesRaw = json_decode(preg_replace('#// Epitech JSON webservice ...#', '', $modulesRaw['response']));

		$modules = [];
		foreach ($modulesRaw as $moduleRaw)
		{
			if ($semester == NULL || $semester == $moduleRaw->semester)
			{
				$row = [
					'title' => $moduleRaw->title,
					'semester' => $moduleRaw->semester,
					'start' => $moduleRaw->begin,
					'end' => $moduleRaw->end,
					'end_register' => $moduleRaw->end_register,
					'code' => $moduleRaw->code,
					'code_instance' => $moduleRaw->codeinstance,
					'credits' => $moduleRaw->credits,
			  ];

				$infos = Module::where('code', $moduleRaw->code)->where('code_instance', $moduleRaw->codeinstance)->first();
				if (!empty($infos))
				{
					$row['already_imported'] = true;
					$row['last_update'] = $infos->last_update;
				}
				else
				{
					$row['already_imported'] = false;
					$row['last_update'] = NULL;
				}
				$modules[] = $row;
			}
		}

		return ($modules);
	}

}
