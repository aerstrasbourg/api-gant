<?php namespace App\Http\Controllers;

use App, Input;
use App\Module;

class GantController extends Controller
{

	public function __construct()
	{
	}

	public function show()
	{
		$params = Input::get('params');
		// Real datas or test datas...
		$params = !empty($params) ? json_decode($params) : (object)[
			'modules' => [
				(object)['code' => 'B-MUL-150', 'code_instance' => 'STG-2-1'],
				(object)['code' => 'B-CPE-155', 'code_instance' => 'STG-2-1'],
				(object)['code' => 'B-PSU-155', 'code_instance' => 'STG-2-1'],
				(object)['code' => 'B-PAV-360', 'code_instance' => 'STG-4-1'],
				(object)['code' => 'B-PSU-335', 'code_instance' => 'STG-4-1'],
				(object)['code' => 'B-PAV-330', 'code_instance' => 'STG-4-1'],
				(object)['code' => 'B-PSU-360', 'code_instance' => 'STG-4-1'],
			],
		];

		/*
		** Creating un-existing modules and updating old ones
		*/
		foreach ($params->modules as $moduleRaw)
		{
			$module = Module::where('code', $moduleRaw->code)->where('code_instance', $moduleRaw->code_instance)->first();

			if (empty($module))
			{
				$module = new Module();
				$module->code = $moduleRaw->code;
				$module->code_instance = $moduleRaw->code_instance;
				$module->save();
			}
			$module->refresh();
		}

		/*
		** Getting all projects from modules
		*/
		$now = new \DateTime();
		$projects = [];
		foreach ($params->modules as $moduleRaw)
		{
			$module = Module::with('projects')->where('code', $moduleRaw->code)->where('code_instance', $moduleRaw->code_instance)->first();
			foreach ($module->projects as $project)
			{
				if ($now >= new \DateTime($project->start) && $now <= new \DateTime($project->end))
				{
					$projects[] = [
						'title' => $project->title,
						'start' => $project->start,
						'end' => $project->end,
						'percent' => floor((time() - strtotime($project->start)) / (strtotime($project->end) - strtotime($project->start)) * 100),
					];
				}
			}
		}

		return ($projects);
	}

	public function refresh()
	{
		$modules = Module::get();

		foreach ($modules as $module)
		{
			$module->refresh(true);
		}
	}

}
